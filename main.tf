terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "~> 3.0.1"
    }
  }
}

provider "docker" {
  #   host = var.address_docker_daemon
}

# ---------------- #
#   Pulls images   #
# ---------------- #

resource "docker_image" "mysql_image" {
  name = "mysql:8.3.0"
  #   keep_locally = false
}

resource "docker_image" "pma_image" {
  name = "phpmyadmin:apache"
  #   keep_locally = false
}

resource "docker_image" "maildev_image" {
  name = "maildev/maildev"
  #   keep_locally = false
}

resource "docker_image" "api_image" {
  name         = "hmveng/todo-app-api:latest"
  keep_locally = false
}

resource "docker_image" "front_image" {
  name         = "hmveng/todo-app-front:latest"
  keep_locally = false
}

# ----------------- #
#    Run network    #
# ----------------- #

resource "docker_network" "app_network" {
  name = "todoappnet"

}

# ----------------- #
#    Containers     #
# ----------------- #

resource "docker_container" "mysql_db" {
  image = docker_image.mysql_image.image_id
  name  = var.bd_container_name
  env = [
    "MYSQL_ALLOW_EMPTY_PASSWORD='yes'"
  ]
  mounts {
    type   = "volume"
    target = "/var/lib/mysql"
    source = "db"
  }
  network_mode = docker_network.app_network.name
  networks_advanced {
    name = docker_network.app_network.name
  }

}

resource "docker_container" "pma" {
  image = docker_image.pma_image.image_id
  name  = var.pma_container_name
  env = [
    "PMA_HOST=${docker_container.mysql_db.name}"
  ]
  ports {
    internal = 80
    external = var.pma_port
  }
  depends_on   = [docker_container.mysql_db]
  network_mode = docker_network.app_network.name
  networks_advanced {
    name = docker_network.app_network.name
  }
}

resource "docker_container" "maildev" {
  image        = docker_image.maildev_image.image_id
  name         = var.maildev_container_name
  command      = ["bin/maildev", "--web", "80", "--smtp", "25", "--hide-extensions", "STARTTLS"]
  network_mode = docker_network.app_network.name
  networks_advanced {
    name = docker_network.app_network.name
  }
  ports {
    internal = 80
    external = var.maildev_port
  }
}

resource "docker_container" "backend" {
  image = docker_image.api_image.image_id
  name  = var.backend_container_name
  ports {
    internal = 80
    external = var.backend_port
  }
  env = [
    "DATABASE_URL=mysql://root:@${docker_container.mysql_db.name}:${var.db_port}/todo_bd"
  ]
  depends_on   = [docker_container.mysql_db]
  network_mode = docker_network.app_network.name
  networks_advanced {
    name = docker_network.app_network.name
  }
}

resource "null_resource" "cmd_target" {
  depends_on = [docker_container.backend]

  provisioner "local-exec" {
    command = "docker exec ${docker_container.backend.name} composer init-db"
  }

  provisioner "local-exec" {
    command = "docker exec ${docker_container.backend.name} composer config-app"
  }

  #   provisioner "local-exec" {
  #     command = "docker exec ${docker_container.backend.name} composer exec-tasks"
  #   }
}


resource "docker_container" "frontend" {
  image = docker_image.front_image.image_id
  name  = var.frontend_container_name
  ports {
    internal = 80
    external = var.app_port
  }
  env = [
    "API_URL=http://${var.backend_ip_address}:${var.backend_port}/api"
  ]
  depends_on = [docker_container.backend, null_resource.cmd_target]

  network_mode = docker_network.app_network.name
  networks_advanced {
    name = docker_network.app_network.name
  }
}