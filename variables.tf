
variable "address_docker_daemon" {
  description = "Value of the default address for the docker daemon"
  type        = string
  default     = "unix:///var/run/docker.sock"
}

variable "bd_container_name" {
  description = "Value of the name for the database container"
  type        = string
  default     = "mysql_db"
}

variable "pma_container_name" {
  description = "Value of the name for the phpmyadmin container"
  type        = string
  default     = "pma"
}

variable "maildev_container_name" {
  description = "Value of the name for the maildev container"
  type        = string
  default     = "maildev_app"
}

variable "backend_container_name" {
  description = "Value of the name for the backend container"
  type        = string
  default     = "backend"
}

variable "frontend_container_name" {
  description = "Value of the name for the frontend container"
  type        = string
  default     = "app"
}

variable "app_port" {
  description = "Value of the app port"
  type        = number
  default     = 6000
}

variable "backend_port" {
  description = "Value of the backend port"
  type        = number
  default     = 6001
}

variable "pma_port" {
  description = "Value of the backend port"
  type        = number
  default     = 6002
}

variable "maildev_port" {
  description = "Value of the backend port"
  type        = number
  default     = 6003
}

variable "db_port" {
  description = "Value of the db port"
  type        = number
  default     = 3306
}

variable "backend_ip_address" {
  description = "Value of the backend ip address"
  type        = string
  default     = "127.0.0.1"
}