# Todo API

This is todo-app infrastructure provosionning by docker

## Features

- Creates users
- Log as user (Get jwt for below features)
- Refresh token (jwt)
- Get Todos (private and public): public todos is for others users
- Create todos (private or public)
- Update todos
- Change todo status 
- Delete todo

## Run Locally

```bash
  terraform init
```
```bash
  terraform plan
```
```bash
  terraform apply
```

## Inspect state
```bash
  terraform show
```

via the Browser

```bash
  APP: http://localhost:6000 (admin account: joe.developer@test.com/0000)
  API: http://localhost:6001/api
  PMA: http://localhost:6002/
  MailDev: http://localhost:6003/#/
```
