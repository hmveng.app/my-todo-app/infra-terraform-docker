output "app_url" {
  description = "APP url"
  value       = "http://localhost:${var.app_port}"
}

output "backend_url" {
  description = "API url"
  value       = "http://localhost:${var.backend_port}/api"
}

output "pma_url" {
  description = "PMA Url"
  value       = "http://localhost:${var.pma_port}"
}

output "mail_app" {
  description = "maildev Url"
  value       = "http://localhost:${var.maildev_port}/#/"
}
